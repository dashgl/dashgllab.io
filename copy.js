"use strict";

const request = require('sync-request');
const format = require('html-format');
const fs = require('fs');

const urls = {
	"/index.html" : "https://gtk.dashgl.com/",
	"/Brickout/index.html" : "https://gtk.dashgl.com/",
	"/Brickout/Introduction/index.html" : "https://gtk.dashgl.com/",
	"/Brickout/Open_a_Window/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Open_a_Window",
	"/Brickout/Draw_a_Triangle/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Draw_a_Triangle",
	"/Brickout/Separate_Shaders/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Separate_Shaders",
	"/Brickout/Shader_Program/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Shader_Program",
	"/Brickout/Orthagonal_Coordinates/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Orthagonal_Coordinates",
	"/Brickout/Draw_a_Ball/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Draw_a_Ball",
	"/Brickout/Set_Ball_Position/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Set_Ball_Position",
	"/Brickout/Move_a_Ball/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Move_a_Ball",
	"/Brickout/Bounce_a_Ball/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Bounce_a_Ball",
	"/Brickout/Define_Ball_Struct/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Define_Ball_Struct",
	"/Brickout/Draw_a_Paddle/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Draw_a_Paddle",
	"/Brickout/Paddle_Uniform_Color/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Paddle_Uniform_Color",
	"/Brickout/Keydown_Callbacks/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Keydown_Callbacks",
	"/Brickout/Paddle_Hit_Detection/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Paddle_Hit_Detection",
	"/Brickout/Draw_Row_of_Bricks/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Draw_Row_of_Bricks",
	"/Brickout/Draw_Grid_of_Bricks/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Draw_Grid_of_Bricks",
	"/Brickout/Break_Some_Bricks/index.html" : "https://gtk.dashgl.com/?folder=Brickout&file=Break_Some_Bricks",
	"/Invaders/index.html" : "https://gtk.dashgl.com/?folder=Invaders",
	"/Invaders/Introduction/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Introduction",
	"/Invaders/Open_a_Window/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Open_a_Window",
	"/Invaders/Draw_a_Triangle/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Draw_a_Triangle",
	"/Invaders/Shader_Program/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Shader_Program",
	"/Invaders/Orthographic_Coordinates/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Orthographic_Coordinates",
	"/Invaders/Cover_Background/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Cover_Background",
	"/Invaders/Background_Sprite/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Background_Sprite",
	"/Invaders/Matrix_Uniform/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Matrix_Uniform",
	"/Invaders/Player_Sprite/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Player_Sprite", 
	"/Invaders/Move_Player_Sprite/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Move_Player_Sprite",
	"/Invaders/Move_Player_Sprite/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Move_Player_Sprite",
	"/Invaders/Draw_Static_Bullet/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Draw_Static_Bullet",
	"/Invaders/Shoot_Player_Bullets/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Shoot_Player_Bullets",
	"/Invaders/Draw_Static_Enemy/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Draw_Static_Enemy",
	"/Invaders/Draw_Static_Enemies/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Draw_Static_Enemies",
	"/Invaders/Move_Enemies/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Move_Enemies",
	"/Invaders/Enemy_Hit_Detection/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Enemy_Hit_Detection",
	"/Invaders/Enemies_Shoot_Back/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Enemies_Shoot_Back",
	"/Invaders/Player_Hit_Detection/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Player_Hit_Detection",
	"/Invaders/Gameover_and_Restart/index.html" : "https://gtk.dashgl.com/?folder=Invaders&file=Gameover_and_Restart",
	"/Astroids/index.html" : "https://gtk.dashgl.com/?folder=Astroids",
	"/Astroids/Introduction/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Introduction",
	"/Astroids/Open_a_Window/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Open_a_Window",
	"/Astroids/Draw_a_Triangle/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Draw_a_Triangle",
	"/Astroids/Shader_Program/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Shader_Program",
	"/Astroids/Orthographic_Coordinates/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Orthographic_Coordinates",
	"/Astroids/Draw_Player_Sprite/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Draw_Player_Sprite",
	"/Astroids/Turn_Left_and_Right/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Turn_Left_and_Right",
	"/Astroids/Move_Player_Sprite/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Move_Player_Sprite",
	"/Astroids/Player_Acceleration/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Player_Acceleration",
	"/Astroids/Player_Position/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Player_Position",
	"/Astroids/Draw_Edge_Sprites/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Draw_Edge_Sprites",
	"/Astroids/Shoot_Bullets/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Shoot_Bullets",
	"/Astroids/Fragment_Shader/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Fragment_Shader",
	"/Astroids/Draw_Astroid/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Draw_Astroid",
	"/Astroids/Astroid_Size/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Astroid_Size",
	"/Astroids/Shoot_Astroid/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Shoot_Astroid",
	"/Astroids/Split_Astroids/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Split_Astroids",
	"/Astroids/Gameover_Screen/index.html" : "https://gtk.dashgl.com/?folder=Astroids&file=Gameover_Screen",
	"/Information/index.html" : "https://gtk.dashgl.com/?folder=Information",
	"/Information/About/index.html" : "https://gtk.dashgl.com/?folder=Information&file=About",
	"/Information/Contribute/index.html" : "https://gtk.dashgl.com/?folder=Information&file=Contribute",
	"/Information/Libraries/index.html" : "https://gtk.dashgl.com/?folder=Information&file=Libraries",
	"/Information/OpenGL_Resources/index.html" : "https://gtk.dashgl.com/?folder=Information&file=OpenGL_Resources",
	"/Information/GTK_Resources/index.html" : "https://gtk.dashgl.com/?folder=Information&file=GTK_Resources",
	"/Information/Copyright/index.html" : "https://gtk.dashgl.com/?folder=Information&file=Copyright",
	"/Information/Mascot/index.html" : "https://gtk.dashgl.com/?folder=Information&file=Mascot",
	"/Information/Raspberry_Pi/index.html" : "https://gtk.dashgl.com/?folder=Information&file=Raspberry_Pi"
}

for(let key in urls) {
	
	console.log(key);

	// First we request the page

	let path = "public" + key;
	let url = urls[key];
	let res = request('GET', url);

	let raw = res.getBody().toString();
	let clean = format(raw);

	clean = clean.replace('img/favicon.png', '/img/favicon.png');
	clean = clean.replace('css/layout.css', '/css/layout.css');
	clean = clean.replace(/src=\"img/g, 'src="/img');

	while(clean.indexOf("?folder=") !== -1) {
		clean = clean.replace("?folder=", "/");
	}

	while(clean.indexOf("&file=") !== -1) {
		clean = clean.replace("&file=", "/");
	}

	let parts = key.split("/");
	let folder = "public";
	for(let i = 0; i < parts.length - 1; i++) {
		folder += "/" + parts[i];
		if(fs.existsSync(folder)){
			continue;
		}
		fs.mkdirSync(folder);
	}

	fs.writeFileSync(path, clean);

}
